## Intro

RPP uses characters that are hard to type over serial (such as 0x01) and so
a GUI was developed to allow testing of peripherals.

This module uses pyrpp, so to clone it use:
```
git clone --recursive git@gitlab.com:robotperipheralprotocol/pyrpp.git
```
Or after cloning it do:
```
git submodule update --init --recursive
```

## Dependencies

* pyserial
* pyrpp (is a git submodule, see above)
* GTK3.0 and pyGTK

## Basic Use
Here is the interface:
![Interface](https://s14.postimg.org/a6f4dza9d/window.png)
The blue box is the baud rate
The red box is the serial port
The green box is used to set values

Clicking on items in the tree view will update their value