import pyrpp
from serial.tools import list_ports

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject


class RppClientGui(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="RPP Browser")
        main_layout = Gtk.VBox(spacing=6)
        self.add(main_layout)
        connect_row = self.create_connect_row()
        middle_row = self.create_middle_row()
        bottom_row = self.create_bottom_row()
        main_layout.pack_start(connect_row, False, False, 0)
        main_layout.pack_start(middle_row, True, True, 0)
        main_layout.pack_start(bottom_row, False, False, 0)
        self.timer_mainloop = GObject.timeout_add(100, self.mainloop)
        self.module = None

    def mainloop(self):
        # Runs every 100ms
        if self.module is not None:
            try:
                self.module.checkAsync()
            except pyrpp.client.ClientError as e:
                if e.response.code != 408:  # timeout
                    RppErrorDialog(self, e)
            except pyrpp.client.ResponseError as e:
                RppErrorDialog(self, e)
        return True

    def create_middle_row(self):
        '''Creates space for the directory tree and
        sets up the tree's renderer'''
        directory_listing_box = Gtk.ScrolledWindow()
        directory_listing_box.set_min_content_height(200)
        viewport = Gtk.Viewport()
        directory_listing_box.add(viewport)
        self.directory_listing = Gtk.TreeView()
        viewport.add(self.directory_listing)

        uricolumn = Gtk.TreeViewColumn('URI')
        text_cell = Gtk.CellRendererText()
        uricolumn.pack_start(text_cell, True)
        uricolumn.add_attribute(text_cell, 'text', 0)
        self.directory_listing.append_column(uricolumn)
        self.directory_listing.connect(
            "cursor-changed",
            self.on_selection_changed
        )
        valuecolumn = Gtk.TreeViewColumn('Most Recent Value')
        text_cell = Gtk.CellRendererText()
        valuecolumn.pack_start(text_cell, True)
        valuecolumn.add_attribute(text_cell, 'text', 1)
        self.directory_listing.append_column(valuecolumn)
        return directory_listing_box

    def create_bottom_row(self):
        '''Creates space for the GET/PUT toolbar'''
        row = Gtk.Box(spacing=6)
        get_button = Gtk.Button(
            label="GET",
            tooltip_text="Retrieves a value from the module"
        )
        get_button.connect("clicked", self.on_get_clicked)
        self.element_value = Gtk.Entry()
        put_button = Gtk.Button(
            label="PUT",
            tooltip_text="Sends a value to the module"
        )
        put_button.connect("clicked", self.on_put_clicked)
        row.pack_start(self.element_value, True, True, 0)
        row.pack_start(get_button, False, False, 0)
        row.pack_start(put_button, False, False, 0)
        return row

    def create_connect_row(self):
        '''Creates the row that allows the user to connect'''
        row = Gtk.Box(spacing=6)
        self.baud_box = Gtk.Entry(tooltip_text="Baud Rate")
        self.baud_box.set_text("115200")
        self.baud_box.connect("changed", self.on_baud_changed)
        self.connect_port_selection = Gtk.ComboBoxText(
            tooltip_text="Serial Port"
        )
        self.connect_port_selection.connect("changed", self.do_connect)

        self.connect_port_selection.set_entry_text_column(0)
        i = 0
        for port in get_port_listing():
            device = port.device
            i += 1
            self.connect_port_selection.append('asdf', device)
        row.pack_start(self.baud_box, False, False, 0)
        row.pack_start(self.connect_port_selection, True, True, 0)
        return row

    def update_directory_tree(self):
        '''Builds the directory tree model'''
        def recursive_add(model, listing, parent=None):
            later = list()
            for entry in sorted(listing.keys()):
                raw = listing[entry]
                if isinstance(raw, dict):
                    new_parent = model.append(parent, [entry, ''])
                    recursive_add(model, raw, new_parent)  # Recursion!
                else:
                    later.append(entry)  # So directories come first

            for entry in later:  # And files later
                model.append(parent, [entry, ''])

        if self.module is not None:
            self.model = Gtk.TreeStore(str, str)
            recursive_add(self.model, self.module.listing)
            self.directory_listing.set_model(self.model)
        else:
            self.model = Gtk.TreeStore(str, str)
            self.directory_listing.set_model(self.model)

    def on_baud_changed(self, widget):
        '''Runs when the baud box is changed, used to filter out non numeric
        characters'''
        out_str = ""
        for char in widget.get_text():
            if char.isdigit():
                out_str += char
        widget.set_text(out_str)

    def do_connect(self, widget):
        '''Performs a connection or throws an error'''
        port = self.connect_port_selection.get_active_text()
        baud = int(self.baud_box.get_text())
        if port is not None:
            try:
                if self.module is not None:
                    self.module.close()
                    self.module = None
                self.module = pyrpp.module.RppModule(port, baud)
            except pyrpp.client.ResponseError as e:
                RppErrorDialog(self, e)
            except pyrpp.client.ClientError as e:
                RppErrorDialog(self, e)
            except Exception as e:
                ErrorDialog(self, e)
                raise e
            self.update_directory_tree()

    def on_selection_changed(self, widget):
        '''Refreshes leaf nodes when clicking around the directory tree'''
        model, treeiter = self.directory_listing.get_selection().get_selected()
        if treeiter is not None:
            if len(list(model[treeiter].iterchildren())) > 0:
                return
            full_path = self.get_full_path(model, treeiter)
            try:
                resp = self.module.client.get(full_path)
                model[treeiter][1] = resp.body
            except pyrpp.client.ResponseError as e:
                model[treeiter][1] = str(e)
                # Fails silently compared to on_get_clicked

    def on_get_clicked(self, widget):
        '''Refreshes a URI's value when clicking get with a URI selected'''
        model, treeiter = self.directory_listing.get_selection().get_selected()
        if treeiter is not None:
            full_path = self.get_full_path(model, treeiter)
            try:
                resp = self.module.client.get(full_path)
                model[treeiter][1] = resp.body
                self.element_value.set_text(resp.body)
            except pyrpp.client.ResponseError as e:
                model[treeiter][1] = str(e)
                # Throw a noisy error to let people know what failed
                RppErrorDialog(self, e)

    def get_full_path(self, model, treeiter):
        '''Turns a TreeView model into a slash-seperated path'''
        parent = model[treeiter]
        full_path = ''
        while parent is not None:
            full_path = '/' + parent[0] + full_path
            parent = parent.get_parent()
        return full_path

    def on_put_clicked(self, widget):
        '''PUT's a new value out towards the peripheral'''
        model, treeiter = self.directory_listing.get_selection().get_selected()
        if treeiter is not None:
            full_path = self.get_full_path(model, treeiter)
            val = self.element_value.get_text()
            try:
                resp = self.module.client.put(full_path, val)
                model[treeiter][1] = resp.body
            except pyrpp.client.ResponseError as e:
                RppErrorDialog(self, e)


class RppErrorDialog:
    '''Error dialog for RppClient and RppResponse errors'''
    def __init__(self, parent, error):
        self.box = Gtk.MessageDialog(
            parent=parent,
            type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.CLOSE,
            message_format=error.get_short_error()
        )
        self.box.format_secondary_text(
            error.response.uri + ': ' + error.response.body
        )
        self.box.connect("response", self.dialog_response)
        self.box.show()

    def dialog_response(self, widget, response_id):
        widget.destroy()


def get_port_listing():
    return list_ports.comports()


class ErrorDialog:
    '''Dumb error dialog'''
    def __init__(self, parent, errorbig, errorsmall=''):
        self.box = Gtk.MessageDialog(
            parent=parent,
            type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.CLOSE,
            message_format=errorbig
        )
        self.box.format_secondary_text(errorsmall)
        self.box.connect("response", self.dialog_response)
        self.box.show()

    def dialog_response(self, widget, response_id):
        widget.destroy()


if __name__ == '__main__':
    win = RppClientGui()
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
